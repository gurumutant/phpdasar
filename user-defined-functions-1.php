<?php 
// User-defined functions
// contoh tanpa menggunakan 
echo "Hello World!\n";
// function tanpa argument/parameter, tanpa return value
function hello() {
    echo "Hello World!\n";
}
// berapa kalipun kita panggil function tersebut,
// dapat digunakan berulang kali
hello(); 
hello(); 
hello(); 