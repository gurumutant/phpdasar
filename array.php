<?php 
    // pembuatan array di PHP (sintaks lama)
    $buah = array("pepaya","mangga","pisang","jambu");
    // pembuatan array sintaks baru
    $hari = ["","Senin", "Selasa", "Rabu","Kamis","Jumat","Sabtu","Minggu"];
    $bulan = ["","Januari","Februari","Maret","April","Mei",
              "Juni","Juli","Agustus","September","Oktober","November",
              "Desember"];
    // pengaksesan elemen array
    echo "Elemen ke 3 array buah adalah: ".$buah[2]."\n";  
    echo "Hari ke-5 adalah : ".$hari[5]."\n";  
    echo "Hari ini adalah : ".$hari[date('N')]."\n";  
    echo "Bulan : ".$bulan[date('n')]."\n";  
    // menampilkan struktur array
    echo "<pre>";
    print_r($buah);
    echo "</pre>";



