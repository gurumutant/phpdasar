<?php
// mengenal fungsi terkait dengan string di PHP

// mengetahui jumlah huruf pada string tertentu
$pesan = 'Selamat hari Jum\'at';
$pesan2 = "Saya bilang, \"Hello World!\"";
echo "Pesan memiliki " . strlen($pesan). " huruf\n";
echo "Pesan memiliki " . strlen($pesan2). " huruf\n";
// mengambil sebagian teks
echo "5 huruf awal dari pesan adalah " . substr($pesan, 0, 5);
echo "\nambil huruf dari indeks 8, sebanyak 4 huruf: ";
echo substr($pesan, 8, 4);
echo "\n".substr("abcdef",3);
$teks = "\nLorem ipsum dolor sit amet, consectetur adipiscing elit.\n Etiam aliquet enim et erat sagittis, vitae suscipit mauris tempus.\n Vivamus facilisis dui sed lacus maximus rutrum.\n Donec facilisis, tortor id euismod elementum, nisl tellus finibus libero, sed volutpat erat libero id orci.\n In dignissim rutrum quam, quis lacinia nulla tristique id. Proin viverra elit non dui luctus, sit amet ullamcorper mauris volutpat. Mauris non arcu consectetur, sollicitudin dui rutrum, gravida enim. Nam semper, urna non ultricies vehicula, tellus neque maximus ligula, vestibulum imperdiet tellus ipsum vel urna. Etiam commodo tellus eu felis tincidunt, quis interdum velit imperdiet. Curabitur vehicula diam est, et posuere lectus lobortis in. Duis feugiat tellus nec massa vestibulum aliquam. Ut elementum tristique odio, ac cursus nisi. Nullam est orci, vehicula ut rhoncus vel, lacinia sed lorem.";
echo substr($teks,0,99);
// $pecah = explode(" ", $teks);
// echo "<pre>";
// print_r($pecah);
// echo "</pre>";
// echo "Jumlah kata pada \$teks: ". count($pecah);
// echo implode("-", $pecah);


?>