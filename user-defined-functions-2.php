<?php 
// User-defined functions - menggunakan argument,
// tanpa return value
function jumlah($a=0,$b=0) {
    echo "hasil penjumlahan argument a dan b: ". ($a+$b) . "\n";
}
jumlah();
jumlah(7,12);
// menampilkan Hello diikuti nama yang dilewatkan di argument
function hello($nama="World") {
    echo "Hello, ". $nama . " !\n"; 
}
hello("Hendri"); hello("Bambang"); hello();
// function untuk menampilkan tanda - sejumlah n kali 
function dash($n=1) {
    $i = 0;
    while ($i < $n) { echo "-"; $i++; }
    echo "\n";
}
dash(5); dash(66); dash();