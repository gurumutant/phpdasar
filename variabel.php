<?php 
    // variabel di PHP selalu diawali dengan tanda $
    $a = 1; $b = 4;
    $c = $a + $b;
    var_dump($c);
    // penggunaan petik dua dan petik satu memberi hasil berbeda
    // tidak hanya pada variabel, tapi juga special character
    echo "Hasil $a ditambah $b = $c \n";
    echo 'Hasil $a ditambah $b = $c \n';
    // tipe data di PHP lebih fleksibel, tidak perlu dideklarasikan
    $c = "Saiki isine teks\n";
    var_dump($c);
    echo "\n isi variabel c sekarang: $c";
?>