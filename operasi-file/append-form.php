<?php 
    // mendeteksi pengiriman form
    if (isset($_POST['submit'])) { 
        $f = fopen('datasiswa.csv', 'a') or die("File tidak ditemukan!");
        $d = ","; // delimiter, pemisah string
        $nama = $_POST['nama'];
        $kelas = $_POST['kelas'];
        $jurusan = $_POST['jurusan'];           
        $data = $nama . $d . $kelas . $d . $jurusan . PHP_EOL;
        if(fwrite($f, $data) === FALSE) {
            die("File gagal ditulis!");
        } else {
            echo "Penambahan teks ke file berhasil.";
            echo "Teks yang ditambahkan : \n". $data;
        }
        fclose($f);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.js"></script>    
</head>
<body>
    <form method="post">
        Nama : 
        <input type="text" name="nama"><br>
        Kelas :
        <input type="text" name="kelas"><br>
        Jurusan :
        <select name="jurusan">
            <option value="DPIB">DPIB</option>
            <option value="RPL">RPL</option>
            <option value="TKJ">TKJ</option>
            <option value="MM">MM</option>
            <option value="ANM">ANM</option>
            <option value="DKV">DKV</option>
            <option value="KKBT">KKBT</option>
            <option value="KKKR">KKKR</option>
            <option value="KKKI">KKKI</option>
            <option value="TKKR">TKKR</option>
            <option value="TB">TB</option>
            <option value="UPW">UPW</option>
        </select><br>
        <input type="submit" name="submit" value="Simpan">
    </form>
    <?php 
        $f = fopen('datasiswa.csv', 'r') or die('file gagal dibuka');
        $rows = [];
        while(!feof($f)) {
            $r = explode(',', fgets($f));
            if (count($r) == 3) {
                $rows[] = $r;
            }   
        }
            
        fclose($f);    
        
    ?>
    <table id="tabelsiswa">
      <thead>  
        <tr>
            <th>Nama</th>
            <th>Kelas</th>
            <th>Jurusan</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($rows as $row): ?>
        <tr>
            <td><?= $row[0]; ?></td>
            <td><?= $row[1]; ?></td>
            <td><?= $row[2]; ?></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>   
    
<script>
$(document).ready( function () {
    $('#tabelsiswa').DataTable();
} );
</script>    
</body>
</html>