<?php 
    // membuka file dan memasukkan ke variabel handle
    if (!file_exists('datasiswa.csv')) die("File tidak ditemukan!");
    $f = fopen('datasiswa.csv', 'a');
    $d = ","; // delimiter, pemisah string

    $nama = "Hendri Winarto";
    $kelas = "XII RPL";
    $jurusan = "RPL";    
    
    $data = $nama . $d . $kelas . $d . $jurusan . PHP_EOL;
    if (is_writable('datasiswa.csv')) {
        if(fwrite($f, $data) === FALSE) {
            die("File gagal ditulis!");
        } else {
            echo "Penambahan teks ke file berhasil.";
            echo "Teks yang ditambahkan : \n". $data;
        }
    } else {
        die('File tidak bisa ditulisi!');
    }    
    fclose($f);
