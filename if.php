<?php
    $nilai = 70;
    if ($nilai >= 70) 
        echo "lulus";
    else 
        echo "tidak lulus";
    echo "\n";
    // sintaks alternatif if-else (Ternary Operator)
    echo ($nilai >= 70) ? "lulus" : "tidak lulus";
?>