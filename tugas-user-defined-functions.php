<?php
function luas_segi3($alas,$tinggi) {
    echo "Luas segitiga dengan alas $alas dan tinggi $tinggi = " 
         . (0.5*$alas*$tinggi) . "\n";
}
function usia($tahun_lahir) {
    return (date('Y')-$tahun_lahir);
}
function nilai_huruf($nilai_angka) {
    if ($nilai_angka > 85) {
        return 'A';
    } elseif ($nilai_angka >= 75) {
        return 'B';
    } elseif ($nilai_angka >= 65) {
        return 'C';
    } elseif ($nilai_angka >= 55) {
        return 'D';
    } else {
        return 'E';
    }
}
function tanggal_indo($tanggal) {
    $bulan = ['',"Januari","Februari","Maret","April","Mei","Juni",
              "Juli","Agustus","September","Oktober","November","Desember"];
    $tgl = explode("-",$tanggal);
    return $tgl[2] . " " . $bulan[(int) $tgl[1]] . " " . $tgl[0];          
}
function tanggal_indo_v2($tanggal) {
    $hari = ["Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu"];
    $bulan = ['',"Januari","Februari","Maret","April","Mei","Juni",
              "Juli","Agustus","September","Oktober","November","Desember"];
    $tgl = DateTime::createFromFormat('Y-m-d', $tanggal);
    return $hari[$tgl->format('w')] .", " . $tgl->format('j') 
           . " " . $bulan[$tgl->format('n')] . " " . $tgl->format('Y');          
}

luas_segi3(4,5);
echo "Usia orang yang lahir di tahun 2000 adalah: " . usia(2000) ."\n";
echo "Nilai huruf untuk nilai 78 adalah: " . nilai_huruf(78) ."\n";
echo "Indonesia merdeka pada tanggal: ". tanggal_indo('1945-08-17') ."\n";
echo "hari ini adalah : ". tanggal_indo_v2('2022-01-05') ."\n";
// contoh type casting:
$bil1 = "04"; var_dump($bil1);
$bil2 = (int) $bil1; var_dump($bil2);