<?php
include "class.mobil.php";

$m1 = new Mobil("Toyota"); // object instantiation mobil ke-1
$m1->set_model("Corolla"); // pemanggilan method pada object
echo "Model Mobil m1 : " . $m1->get_model() . PHP_EOL;
$m2 = new Mobil("Lamborghini"); // object instantiation mobil ke-2
$m2->set_model("Aventador"); // pemanggilan method pada object
echo "Model Mobil m2 : " . $m2->get_model() . PHP_EOL;
// perbedaan access modifier public dan private
// $m1->merk = "Daihatsu";
echo "Mobil m1 bermerk : " . $m1->merk . PHP_EOL;
// mencoba mengeset model mobil secara langsung (memicu error,
// karena access modifier-nya private)
// $m1->model = "Camry";
echo "Model Mobil m1 sekarang : " . $m1->get_model() . PHP_EOL;
// praktek memanfaatkan class turunan
$m3 = new Pickup("Daihatsu");
$m3->set_model("Gran Max");
echo "m3 merupakan mobil dengan merk " . $m3->merk . " dan modelnya : " .
     $m3->get_model() . PHP_EOL;
$m3->set_tahun_produksi("2019");
echo "m3 diproduksi tahun " . $m3->get_tahun_produksi();