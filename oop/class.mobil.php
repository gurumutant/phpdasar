<?php 
class Mobil {
    // pendeklarasian class member: properties
    public $merk; 
    private $model; 
    protected $tahun_produksi;
    private $kubikasi_mesin; 
    private $jumlah_pintu;

    // pendefinisian constructor, dijalankan saat instatiation
    function __construct($merk) {
        $this->merk = $merk;
    }

    // pendefinisian class member: methods
    public function set_model($model) {
        $this->model = $model;
    }
    public function get_model() {
        return $this->model;
    }
}

class Pickup extends Mobil {
    public $panjang_bak;
    public $lebar_bak;
    
    function set_tahun_produksi($th) {
        $this->tahun_produksi = $th;
    }
    function get_tahun_produksi() {
        return $this->tahun_produksi;
    }
}